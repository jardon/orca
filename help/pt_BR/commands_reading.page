<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="commands_reading" xml:lang="pt-BR">
  <info>
    <link type="next" xref="commands_structural_navigation"/>
    <link type="guide" xref="commands#reading_documents"/>
    <link type="seealso" xref="howto_documents"/>
    <link type="seealso" xref="howto_text_attributes"/>
    <link type="seealso" xref="howto_whereami"/>
    <title type="sort">1. Leitura</title>
    <title type="link">Leitura</title>
    <desc>Comandos para acessar o conteúdo do documento</desc>
    <credit type="author">
      <name>Joanmarie Diggs</name>
      <email>joanied@gnome.org</email>
    </credit>
    <license>
      <p>Atribuição Compartilhada Igual 3.0 — Creative Commons</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Conte</mal:name>
      <mal:email>alente.alemao@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>José Vilmar Estácio De Souza</mal:name>
      <mal:email>vilmar@informal.com.br</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Leônidas Araújo</mal:name>
      <mal:email>leorusvellt@hotmail.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>
  <title>Comandos de leitura</title>
  <p>Além dos comandos de navegação do cursor que fazem parte do GNOME, o <app>Orca</app> fornece uma série de comandos que você pode usar para ler documentos.</p>
  <note style="tip">
    <p>Abaixo você verá várias referências a "NUM". Todas as teclas "NUM" estão localizadas no teclado numérico. Você também notará que há diferentes pressionamentos de tecla, a depender se você está usando um desktop ou um laptop — ou mais precisamente, se você está usando o layout de teclado Desktop ou Laptop do <app>Orca</app>. Para obter informações adicionais, consulte o tópico <link xref="howto_keyboard_layout">Disposição de Teclado</link>.</p>
  </note>
  <section id="flat_review">
    <title>Lendo sua localização atual</title>
    <p>Os <link xref="commands_flat_review">comandos de revisão plana</link> do <app>Orca</app> a seguir podem ser usados para ler sua posição atual:</p>
    <list>
      <item>
        <p>Lê a linha atual:</p>
        <list>
          <item>
            <p>Desktop: <keyseq><key>NUM 8</key></keyseq></p>
          </item>
          <item>
            <p>Notebook: <keyseq><key>Tecla Modificadora do Orca</key><key>I</key></keyseq></p>
          </item>
        </list>
      </item>
      <item>
        <p>Lê a palavra atual:</p>
        <list>
          <item>
            <p>Desktop: <keyseq><key>NUM 5</key></keyseq></p>
          </item>
          <item>
            <p>Notebook: <keyseq><key>Tecla Modificadora do Orca</key><key>K</key></keyseq></p>
          </item>
        </list>
      </item>
      <item>
        <p>Soletra a palavra atual:</p>
        <list>
          <item>
            <p>Desktop: <keyseq><key>NUM 5</key></keyseq> (clique duas vezes)</p>
          </item>
          <item>
            <p>Notebook: <keyseq><key>Tecla Modificadora do Orca</key><key>K</key></keyseq> (clique duas vezes)</p>
          </item>
        </list>
      </item>
      <item>
        <p>Soletra foneticamente a palavra atual:</p>
        <list>
          <item>
            <p>Desktop: <keyseq><key>NUM 5</key></keyseq> (clique três vezes)</p>
          </item>
          <item>
            <p>Notebook: <keyseq><key>Tecla Modificadora do Orca</key><key>K</key></keyseq> (pressionada três vezes)</p>
          </item>
        </list>
      </item>
      <item>
        <p>Lê o caractere atual:</p>
        <list>
          <item>
            <p>Desktop: <keyseq><key>NUM 2</key></keyseq></p>
          </item>
          <item>
            <p>Notebook: <keyseq><key>Tecla Modificadora do Orca</key><key>Vírgula</key></keyseq></p>
          </item>
        </list>
      </item>
      <item>
        <p>Fala foneticamente o caractere atual:</p>
        <list>
          <item>
            <p>Desktop: <keyseq><key>NUM 2</key></keyseq> (clique duas vezes)</p>
          </item>
          <item>
            <p>Notebook: <keyseq><key>Tecla Modificadora do Orca</key><key>Virgula</key></keyseq> (clique duas vezes)</p>
          </item>
        </list>
      </item>
      <item>
        <p>Fala o valor Unicode do caractere atual:</p>
        <list>
          <item>
            <p>Desktop: <keyseq><key>NUM 2</key></keyseq> (clique três vezes)</p>
          </item>
          <item>
            <p>Notebook: <keyseq><key>Tecla Modificadora do Orca</key><key>Vírgula</key></keyseq> (pressionada três vezes)</p>
          </item>
        </list>
      </item>
    </list>
  </section>
  <section id="say_all">
    <title>Ler tudo</title>
    <p>O comando do Orca Ler tudo fará o <app>Orca</app> falar o documento inteiro, iniciando da sua posição atual.</p>
    <list>
      <item>
        <p>Desktop: <key>NUM Mais</key></p>
      </item>
      <item>
        <p>Notebook: <keyseq><key>Tecla Modificadora do Orca</key><key>Ponto e vírgula</key></keyseq></p>
      </item>
    </list>
  </section>
  <section id="attributes_and_selection">
    <title>Atributos do texto e texto selecionado</title>
    <p>O Orca tem um comando específico para obtenção dos atributos do texto na posição do cursor. Além disso, se você usar os comandos Onde Estou do <app>Orca</app> dentro de um objeto de texto no qual o texto tenha sido selecionado, o <app>Orca</app> anunciará o texto selecionado.</p>
    <list>
      <item>
        <p>Apresenta os atributos do texto: <keyseq><key>Tecla Modificadora do Orca</key><key>F</key></keyseq></p>
      </item>
      <item>
        <p>Executa o comando Onde Estou básico:</p>
        <list>
          <item>
            <p>Desktop: <key>NUM Enter</key></p>
          </item>
          <item>
            <p>Notebook: <keyseq><key>Tecla Modificadora do Orca</key><key>Enter</key></keyseq></p>
          </item>
        </list>
    </item>
    <item>
      <p>Executa o comando Onde Estou detalhado:</p>
      <list>
        <item>
          <p>Desktop: <key>NUM Enter</key> (clique duas vezes)</p>
        </item>
        <item>
          <p>Notebook: <keyseq><key>Tecla Modificadora do Orca</key><key>Enter</key></keyseq> (clique duas vezes)</p>
        </item>
      </list>
    </item>
    <item>
      <p>Fala a seleção atual: <keyseq><key>Tecla Modificadora do Orca</key><key>Shift</key><key>Para Cima</key></keyseq></p>
    </item>
  </list>
  </section>
  <section id="link_details">
    <title>Detalhes do Link</title>
    <p>Se você estiver em um link, o comando Onde Estou Básico do <app>Orca</app> pode ser usado para anunciar os detalhes associados ao link, como o tipo de link, se o link foi visitado, a descrição do site e tamanho do arquivo. Se você preferir um comando dedicado para essa finalidade, você pode vincular o comando Falar detalhes do link do <app>Orca</app> a um pressionamento de tecla. Consulte <link xref="howto_key_bindings"> Associações de teclas</link> para obter informações sobre como fazê-lo.</p>
    <list>
      <item>
        <p>Fale os detalhes do link: (Sem atalho definido)</p>
      </item>
    </list>
  </section>
  <section id="browse_and_focus_modes">
    <title>Modos de Navegação e Foco</title>
    <p>Os modos de Navegação e Foco do <app>Orca</app> permitem alternar entre ler e interagir com o conteúdo da web.</p>
    <list>
      <item>
        <p>Alterna entre o modo de navegação e o modo de foco: <keyseq><key>Tecla Modificadora do Orca</key><key>A</key></keyseq></p>
      </item>
      <item>
        <p>Habilita o modo de foco fixo: <keyseq><key>Tecla Modificadora do Orca</key><key>A</key></keyseq> (clique duas vezes)</p>
      </item>
      <item>
        <p>Habilita o modo de navegação fixo: <keyseq><key>Tecla Modificadora do Orca</key><key>A</key></keyseq> (clique três vezes)</p>
      </item>
    </list>
  </section>
  <section id="toggling_layout_mode">
    <title>Alterna o Modo de Layout</title>
    <p>Quando o Modo Layout está ativado, a navegação por cursor do <app>Orca</app> respeitará o layout do conteúdo na tela e apresentará a linha completa, incluindo quaisquer links ou campos de formulário nessa linha. Quando o Modo Layout está desabilitado, o <app>Orca</app> tratará objetos como links e campos de formulário como se estivessem em linhas separadas, tanto para apresentação quanto para navegação.</p>
    <p>O <app>Orca</app> fornece um comando para alternar entre o modo Layout e o modo Objeto. Este comando não possui atalho definido por padrão. Consulte <link xref="howto_key_bindings">Associações de teclas</link> para obter informações sobre como vincular comandos Sem atalho definidos.</p>
    <list>
      <item>
        <p>Alterna entre o Modo de Layout e o Modo de Objeto: (Sem atalho definido)</p>
      </item>
    </list>
  </section>
</page>
