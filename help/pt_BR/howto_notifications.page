<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="howto_notifications" xml:lang="pt-BR">
  <info>
    <link type="guide" xref="index#reviewing"/>
    <link type="next" xref="howto_bookmarks"/>
    <title type="sort">5. Notificações</title>
    <desc>Leitura de mensagens recebidas</desc>
    <credit type="author">
      <name>Joanmarie Diggs</name>
      <email>joanied@gnome.org</email>
    </credit>
    <license>
      <p>Atribuição Compartilhada Igual 3.0 — Creative Commons</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Conte</mal:name>
      <mal:email>alente.alemao@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>José Vilmar Estácio De Souza</mal:name>
      <mal:email>vilmar@informal.com.br</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Leônidas Araújo</mal:name>
      <mal:email>leorusvellt@hotmail.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>
  <title>Notificações</title>
  <p>A área de trabalho do GNOME inclui uma "área de notificação" que pode ser usada por aplicativos para apresentar informação ao usuário. Exemplos de notificações são a chegada de mensagens de bate-papo, os detalhes associados com o som que seu tocador de mídia está reproduzindo, e o aviso que seu nível de bateria está ficando baixo.</p>
  <p>Como as notificações destinam-se a apresentar informações aos usuários sem interromper o que ele está fazendo, essas mensagens tendem a aparecer e desaparecer com relativa rapidez. Elas também não são focalizáveis. Embora o <app>Orca</app> apresente-as para você à medida que elas chegam, é muito fácil interromper o anúncio acidentalmente continuando a digitar ou mudando o foco. Por esse motivo, o <app>Orca</app> possui três comandos que você pode usar para acessar as mensagens de notificação exibidas anteriormente:</p>
  <list>
    <item>
      <p>Apresenta a última (mais recente) mensagem de notificação</p>
    </item>
    <item>
      <p>Apresenta a mensagem de notificação anterior</p>
    </item>
    <item>
      <p>Apresenta uma lista com todas as mensagens de notificação</p>
    </item>
  </list>
  <p>Os ddois primeiros comandos são projetados principalmente para um rápido acesso a uma mensagem que você recém tenha recebido. O último, é o mais poderoso uma vez que ele armazena seu histórico de mensagem de notificação.</p>
  <p>Quando você entrar na lista de mensagens de notificação, você será avisado do tamanho da lista e solicitado a escolher uma das seguintes opções:</p>
  <list>
    <item><p>Pressione H para ajuda.</p></item>
    <item><p>Use seta para cima ou para baixo, home ou end para navegar pela lista.</p></item>
    <item><p>Pressione escapeepara sair.</p></item>
    <item><p>Pressione barra de espaço para repetir a última mensagem lida.</p></item>
    <item><p>Pressione um dígito para ler uma mensagem específica.</p></item>
  </list>
  <p>Observe que a mensagem recebida mais recentemente estará no topo da lista.</p>
  <p>Cada um dos comandos de revisão de notificação do <app>Orca</app> é desvinculado de atalho de teclado por padrão. Você pode vincular qualquer um ou todos eles ao atalho ou atalhos que você escolher. Como fazer isso está descrito na <link xref="howto_key_bindings">Introdução aos Atalhos de Teclado</link>.</p>
</page>
