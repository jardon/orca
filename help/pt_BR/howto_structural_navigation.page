<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="howto_structural_navigation" xml:lang="pt-BR">
  <info>
    <link type="guide" xref="index#reading"/>
    <link type="next" xref="howto_tables"/>
    <title type="sort">3. Navegação Estrutural</title>
    <desc>Movimentando-se por cabeçalhos e outros elementos</desc>
    <credit type="author">
      <name>Joanmarie Diggs</name>
      <email>joanied@gnome.org</email>
    </credit>
    <license>
      <p>Atribuição Compartilhada Igual 3.0 — Creative Commons</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Conte</mal:name>
      <mal:email>alente.alemao@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>José Vilmar Estácio De Souza</mal:name>
      <mal:email>vilmar@informal.com.br</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Leônidas Araújo</mal:name>
      <mal:email>leorusvellt@hotmail.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>
  <title>Navegação estrutural</title>
  <p>O recurso de Navegação Estrutural do <app>Orca</app> permite que você navegue entre os elementos de um documento. Os tipos de elementos pelos quais você pode navegar incluem:</p>
  <list>
    <item><p>Cabeçalhos e outros blocos de texto</p></item>
    <item><p>Controles de formulário</p></item>
    <item><p>Links</p></item>
    <item><p>Listas e itens de listas</p></item>
    <item><p>Marcadores, separadores e âncoras</p></item>
    <item><p>Tabelas e células de tabelas</p></item>
  </list>
  <p>Uma lista completa dos elementos individuais e seus atalhos pode ser encontrada em <link xref="commands_structural_navigation">Comandos de Navegação Estrutural</link>.</p>
  <section id="applications">
      <title>Aplicativos suportados</title>
      <p>Atualmente, a Navegação Estrutural está totalmente implementada para conteúdo da web, incluindo o conteúdo de ajuda que você está lendo agora. O suporte de Navegação Estrutural do <app>Orca</app> para células de tabela também foi implementado no <app>OpenOffice Writer</app> e <app>LibreOffice Writer</app>. A implementação do restante dos objetos de Navegação Estrutural para esses conjuntos de escritório exige que as alterações sejam feitas por seus respectivos desenvolvedores. A implementação de qualquer recurso de Navegação Estrutural no <app>Evince</app> exigirá um esforço semelhante por parte de seus desenvolvedores.</p>
      <note style="tip">
        <title>Não se esqueça de ativar a Navegação Estrutural!</title>
        <p>Dependendo de onde você estiver, pode ser necessário ativar explicitamente a Navegação Estrutural antes de poder usá-la.</p>
      </note>
      <section id="toggling_required">
        <title>Quando a ativação da Navegação Estrutural é exigida</title>
        <p>Em páginas da web, a ativação da Navegação Estrutural geralmente não é necessária tendo em vista que sua interação com o documento consiste, em grande parte, na leitura do seu conteúdo. Assim não há como ter dúvida se o 'H' pressionado por você era um comando de escrita ou de navegação.</p>
        <p>Por outro lado, em documentos editáveis como os encontrados no <app>OpenOffice</app> e <app>LibreOffice</app>, é bem mais difícil para o <app>Orca</app> identificar exatamente o que você esperava que acontecesse como resultado do pressionamento do 'H'. Portanto, antes de você usar qualquer comando da navegação estrutural num documento editável, você precisa primeiro ativá-la pressionando <keyseq><key>Tecla Modificadora do Orca</key><key>Z</key></keyseq>. Quando tiver terminado a navegação e pronto para continuar escrevendo, pressione novamente <keyseq><key>Tecla Modificadora do Orca</key><key>Z</key></keyseq> para desativá-la.</p>
      </section>
  </section>
  <section id="settings">
    <title>Configurações Disponíveis</title>
    <p>Além dos comandos supracitados, o <app>Orca</app> tem uma série de opções configuráveis disponíveis, especificamente para aplicativos nos quais há suporte à navegação estrutural.</p>
    <steps>
      <title>Configurando Navegação Estrutural</title>
      <item>
        <p>Focar a um aplicativo para o qual o <app>Orca</app> possui suporte à Navegação Estrutural.</p>
      </item>
      <item>
        <p>Acesse a caixa de diálogo <link xref="preferences">Preferências do Orca</link> para o aplicativo atual pressionando <keyseq> <key>Ctrl</key><key>Tecla Modificadora do Orca</key><key>Barra de Espaço</key> </keyseq></p>
      </item>
      <item>
        <p>Vá para a última aba da caixa de diálogo que deve ter o nome do seu aplicativo atual.</p>
      </item>
      <item>
        <p>Examine e altere as configurações como achar melhor.</p>
      </item>
      <item>
        <p>Pressione o botão <gui>OK</gui>.</p>
      </item>
    </steps>
  </section>
</page>
